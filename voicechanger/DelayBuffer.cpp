
#include "DelayBuffer.h"
#include <vector>
#include <iostream>

// constructor
DelayBuffer::DelayBuffer(int size) {
    head = samples.size();
    for (int i = 0; i < size; i++) {
        samples.push_back(0);
    }
}


float DelayBuffer::get(int index) {
    // TODO modulo here


    // select the correct sample
    if (index > samples.size()) { // want a sample older than we could possiby have, so modulo it
        /*
        if (head != samples.size()-1) { // head isn't at the end
            return samples[head+1];
        }
        else { // head is at the end, so give samples[0]
            return samples[0];
        }
        */
        return samples.at(index % samples.size());
    }
    else if ((head - index) >= 0) {
        return samples.at(head - index);
    }
    else {
        return samples.at(head - index + samples.size());
    }

}

void DelayBuffer::put(float sample) {
    // advance head, wrap if necessary
    if (head + 1 >= samples.size()) {
        head = 0;
    }
    else {
        head++;
    }

    samples[head] = sample;

}

int DelayBuffer::size() {
    return samples.size();
}
