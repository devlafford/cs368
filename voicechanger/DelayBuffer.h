// this class is a circular buffer that allows you to treat the audio with the semantics:
// "get the sample that is xxx samples old."

#include <vector>

class DelayBuffer {

private:
	int head;
        std::vector<float> samples;

public:
        DelayBuffer(int size);
	float get(int index);
        void put(float sample);
        int size();


};
