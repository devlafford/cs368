// this class is a circular buffer that recreates the sound of yelling into a cave

#include <vector>

class ReverbCave {

private:
   int head;
   std::vector<float> samples;

public:
   ReverbCave(int size);
   float get(int index);
   void put(float sample);
   bool leftRightDelay;
   int volMult;
};
