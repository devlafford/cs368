#include "ReverbTunnel.h"
#include <vector>

// constructor for the tunnel
ReverbTunnel::ReverbTunnel(int size) {
   head = 0;
   for (int i = 0; i < size; i++) {
      samples.push_back(0);
   }
   leftRightDelay = false;


   //volume in dB 0db = unity gain, no attenuation, full amplitude signal
   //           -20db = 10x attenuation, significantly more quiet
   float volumeLevelDb = -6.f; //cut amplitude in half; same as 0.5 above
   float VOLUME_REFERENCE = 1.f;
   float volumeMultiplier = (VOLUME_REFERENCE * pow(10, (volumeLevelDb / 20.f)));
   volMult*= volumeMultiplier;


}

float ReverbTunnel::get(int index) {
   if (index > samples.size()) {
      if (head != samples.size() - 1) {
         return samples[head + 1];
      }
      else { 
         return samples[0];
      }
   }
   else if ((head - index) >= 0) {
      return samples.at(head - index);
   }
   else {
      return samples.at(head - index + samples.size());
   }

}

void ReverbTunnel::put(float sample) {
   if (head + 1 >= samples.size()) {
      head = 0;
   }
   else {
      head++;
   }

   samples[head] = sample;
}
