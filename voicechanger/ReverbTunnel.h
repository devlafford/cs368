// this class is a circular buffer that recreates the sound of yelling down a tunnel

#include <vector>

class ReverbTunnel {

private:
   int head;
   std::vector<float> samples;

public:
   ReverbTunnel(int size);
   float get(int index);
   void put(float sample);
   bool leftRightDelay;
   int volMult;
};
