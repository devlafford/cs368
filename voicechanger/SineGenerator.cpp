#include <math.h>
#include "SineGenerator.h"


SineGenerator::SineGenerator(float hz, int sample_rate, int table_size) {

    // calculate the phase increment
    phase_incr = table_size / (sample_rate / hz);
    current_phase = -phase_incr;

    // create the sinusoidal table
    for( int i = 0; i < table_size; i++ ) {
        sinetable.push_back((float) sin(((double) i / (double) table_size) * M_PI * 2. ));
    }
}

float SineGenerator::next() {
    current_phase += phase_incr;
    if (current_phase >= sinetable.size()-1) current_phase = 0;
    return sinetable[(int) std::round(current_phase)];
}
