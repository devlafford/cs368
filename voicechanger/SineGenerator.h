// this class allows you to construct a sine wave of a given frequency

#include <vector>
#define M_PI 3.14159265

class SineGenerator {

private:
    float current_phase;
    float phase_incr;
    std::vector<float> sinetable;

public: 
    SineGenerator(float hz, int sample_rate, int table_size);
    float next();

};

