#include <stdio.h>
#include <stdlib.h>
#include <string>
#include "portaudio.h"
#include <vector>
#include <queue>
#include <csignal>
#include <iostream>
#include <math.h>

#include "DelayBuffer.h"
#include "SineGenerator.h"
#include "ReverbTunnel.h"
#include "ReverbCave.h"

#define SAMPLE_RATE       (44100)
#define FRAMES_PER_BUFFER   (512)
#define DITHER_FLAG           (0)

#define PA_SAMPLE_TYPE  paFloat32
#define SAMPLE_SIZE (4)
#define SAMPLE_SILENCE  (0.0f)
#define PRINTF_S_FORMAT "%.8f"

typedef struct someData {

    unsigned int noise;
    unsigned int time;
    float scale;
    float shift_amt;
    DelayBuffer& dbl;
    DelayBuffer& dbr;
    SineGenerator& sg;
    SineGenerator& sgflange;
    ReverbTunnel& rvbtl;
    ReverbTunnel& rvbtr;
    ReverbCave& rvbcl;
    ReverbCave& rvbcr;
    std::string& choice;

    someData(
        DelayBuffer& dbl,
        DelayBuffer& dbr,
        SineGenerator& sg,
        SineGenerator& sgflange,
        ReverbTunnel& rvbtl,
        ReverbTunnel& rvbtr,
        ReverbCave& rvbcl,
        ReverbCave& rvbcr,
        std::string& choice) :
        dbl(dbl),
        dbr(dbr),
        sg(sg),
        sgflange(sgflange),
        rvbtl(rvbtl),
        rvbtr(rvbtr),
        rvbcl(rvbcl),
        rvbcr(rvbcr),
        choice(choice)
        {}
} UserData;

// our audio processing function goes here
// if we want multiple effects, we will store a bitfield in userData to determine which effects
static int voiceCallback(const void *inputBuffer, void *outputBuffer,
    unsigned long framesPerBuffer,
    const PaStreamCallbackTimeInfo* timeInfo,
    PaStreamCallbackFlags statusFlags,
    void *userData)
{
    float *in = (float*)inputBuffer; // pointer to inch along the buffer
    float *out = (float*)outputBuffer; // pointer to inch along the buffer

    unsigned int& n = ((UserData*)userData)->noise;
    unsigned int& t = ((UserData*)userData)->time;
    float& scale = ((UserData*)userData)->scale;
    float& shift_amt = ((UserData*)userData)->shift_amt;
    DelayBuffer& dbl = ((UserData*)userData)->dbl;
    DelayBuffer& dbr = ((UserData*)userData)->dbr;
    SineGenerator& sg = ((UserData*)userData)->sg;
    SineGenerator& sgflange = ((UserData*)userData)->sgflange;
    ReverbTunnel& rvbtl = ((UserData*)userData)->rvbtl;
    ReverbTunnel& rvbtr = ((UserData*)userData)->rvbtr;
    ReverbCave& rvbcl = ((UserData*)userData)->rvbcl;
    ReverbCave& rvbcr = ((UserData*)userData)->rvbcr;
    std::string& choice = ((UserData*)userData)->choice;

    // params
    float shift_pos = 0;

    for (unsigned int i = 0; i < framesPerBuffer; i++) {
        // do all once-per-sample operations up here
        float linput = *in++;
        float rinput = *in++;

        float lsample = 0;
        float rsample = 0;
        float sineflange = sgflange.next();
        float sine = sg.next();
        dbl.put(linput);
        dbr.put(rinput);
        rvbtl.put(linput);
        rvbtr.put(rinput);
        rvbcl.put(linput);
        rvbcr.put(rinput);

        // based on input, mutate sound
        if (choice == "N") {
            lsample = dbl.get(0);
            rsample = dbr.get(0);
        }
        else if (choice == "C") {
          float l0 = rvbcl.get(0);
          float r0 = rvbcr.get(0);
          float l1 = rvbcl.get(1000);
          float r1 = rvbcr.get(1000);
          float l2 = rvbcl.get(3000);
          float r2 = rvbcr.get(3000);
          float l3 = rvbcl.get(5000);
          float r3 = rvbcr.get(5000);
          float l4 = rvbcl.get(7000);
          float r4 = rvbcr.get(7000);
          float l5 = rvbcl.get(9000);
          float r5 = rvbcr.get(9000);
          lsample = l0 + l1/6 + l2/6 + l3/6 + l4/6 + l5/10;
          rsample = r0 + r1/6 + r2/6 + r3/6 + r4/6 + r5/10;
        }
        else if (choice == "T") {
          float l0 = rvbtl.get(0);
          float r0 = rvbtr.get(0);
          float l1 = rvbtl.get(10000);
          float r1 = rvbtr.get(10000);
          float l2 = rvbtl.get(20000);
          float r2 = rvbtr.get(20000);
          float l3 = rvbtl.get(30000);
          float r3 = rvbtr.get(30000);
          float l4 = rvbtl.get(40000);
          float r4 = rvbtr.get(40000);
          float l5 = rvbtl.get(50000);
          float r5 = rvbtr.get(50000);
          lsample = l0 + l1/6 + l2/6 + l3/6 + l4/6 + l5/10;
          rsample = r0 + r1/6 + r2/6 + r3/6 + r4/6 + r5/10;
        }
        else if (choice == "S") {
            lsample = dbl.get(0);
            rsample = dbr.get(0);
            lsample /= 2;
            rsample /= 2;
            lsample += sine / 2;
            rsample += sine / 2;
        }
        else if (choice == "P") {
            int index = (int) std::round(shift_pos);
            shift_pos += shift_amt;
            if (shift_pos > scale * (float) dbl.size()) {
                shift_pos = 0;
            }
            if (shift_pos < 0) {
                shift_pos = scale * (float) dbl.size();
            }

            lsample = dbl.get(index);
            rsample = dbr.get(index);
        }
        else if (choice == "U" || choice == "R" || choice == "F") {
            lsample /= 2;
            rsample /= 2;
            int index =  (int) ( ((float) dbl.size() * scale) * (std::abs(sineflange)));
            lsample += dbl.get(index);
            rsample += dbr.get(index);
        }
        else {
            std::cout << "You didn't choose a valid option.\n";
            exit(1);
        }

        // output what we've built
        *out++ = lsample;
        *out++ = rsample;
    }




    /*
    // play a weird periodic noise
    for (unsigned int i = 0; i < framesPerBuffer; i++) {
        n = (n >> 1) + (n >> 4) + (t * (((t >> 15) | (t >> 7)) & (271 & (t >> 5))));
            t++;
            *out++ = (float) (n);
            *out++ = (float) (n);
            //std::cout << n << std::endl;
    }
    */


    return paContinue;

}

void sigintHandler(int signum) {
    std::cout << "Interrupt received. Cleaning up and exiting..." << std::endl;
    std::exit(0);
    // TODO: clean up
}


/*******************************************************************/
int main(void);
int main(void)
{
    std::string choice;
    float flangefreq = 0;
    float shift_amt = 0;
    float sinefreq = 0;
    std::cout << "Welcome to the Voice Changer! :) \n How would you like to change your voice today? \n Experience a: \n";
    std::cout << "N - Normal" << std::endl;
    std::cout << "C - Cave" << std::endl;
    std::cout << "T - Tunnel" << std::endl;
    std::cout << "R - Robot" << std::endl;
    std::cout << "U - Underwater" << std::endl;
    std::cout << "F - Flanger" << std::endl;
    std::cout << "P - Pitch Shift" << std::endl;
    std::cout << "S - Sine Wave" << std::endl;

    std::cin >> choice;

    // set the frequency for the flanger based on the user's choice
    if (choice == "R") flangefreq = 18;
    else if (choice == "U") flangefreq = 6;
    else if (choice == "F") flangefreq = 1;
    else flangefreq = 1;
    
    // set the shift amount based on the user's input
    if (choice == "P") {
        std::cout << "How much do you want your voice shifted? (-1.0 to 0.5)" << std::endl;
        std::cin >> shift_amt;
    }

    // set the sine frequency based on the user's input
    if (choice == "S") {
        std::cout << "What frequency sine wave do you want?" << std::endl;
        std::cin >> sinefreq;
    }

    /* -- portaudio setup -- */
    PaStreamParameters inputParameters, outputParameters;
    PaStream *stream = NULL;
    PaError err;
    const PaDeviceInfo* inputInfo;
    const PaDeviceInfo* outputInfo;
    char *sampleBlock = NULL;
    int i;
    int numBytes;
    int numChannels;

    // create objects we'll need
    DelayBuffer dbl(65536);
    DelayBuffer dbr(65536);
    SineGenerator sg(sinefreq, SAMPLE_RATE, 65536);
    SineGenerator sgflange(flangefreq, SAMPLE_RATE, 65536);
    ReverbTunnel rvbtl(65536);
    ReverbTunnel rvbtr(65536);
    ReverbCave rvbcl(65536);
    ReverbCave rvbcr(65536);

    // our data. it is yet unknown what this should contain.
    UserData userd(dbl, dbr, sg, sgflange, rvbtl, rvbtr, rvbcl, rvbcr, choice);
    userd.noise = 55555555; // some odd number, idk
    userd.time = 0;
    userd.scale = (float) 1/64;
    userd.shift_amt = shift_amt;

    err = Pa_Initialize();
    if( err != paNoError ) goto error2;

    inputParameters.device = Pa_GetDefaultInputDevice(); /* default input device */
    inputInfo = Pa_GetDeviceInfo( inputParameters.device );

    outputParameters.device = Pa_GetDefaultOutputDevice(); /* default output device */
    outputInfo = Pa_GetDeviceInfo( outputParameters.device );

    numChannels = inputInfo->maxInputChannels < outputInfo->maxOutputChannels
            ? inputInfo->maxInputChannels : outputInfo->maxOutputChannels;

    inputParameters.channelCount = numChannels;
    inputParameters.sampleFormat = PA_SAMPLE_TYPE;
    inputParameters.suggestedLatency = inputInfo->defaultHighInputLatency;
    inputParameters.hostApiSpecificStreamInfo = NULL;

    outputParameters.channelCount = numChannels;
    outputParameters.sampleFormat = PA_SAMPLE_TYPE;
    outputParameters.suggestedLatency = outputInfo->defaultHighOutputLatency;
    outputParameters.hostApiSpecificStreamInfo = NULL;

        // open audio stream with our desired function
    err = Pa_OpenStream(
              &stream,
              &inputParameters,
              &outputParameters,
              SAMPLE_RATE,
              FRAMES_PER_BUFFER,
              NULL,
              voiceCallback,
              &userd );
    if( err != paNoError ) goto error2;

    numBytes = FRAMES_PER_BUFFER * numChannels * SAMPLE_SIZE ;
    sampleBlock = (char *) malloc( numBytes );
    if( sampleBlock == NULL )
    {
        printf("Could not allocate record array.\n");
        goto error1;
    }
    memset( sampleBlock, SAMPLE_SILENCE, numBytes );

    err = Pa_StartStream( stream );
    if( err != paNoError ) goto error1;
    printf("Wire on.\n"); fflush(stdout);
    while (1) { ; }

    printf("Wire off.\n"); fflush(stdout);

    err = Pa_StopStream( stream );
    if( err != paNoError ) goto error1;

    free( sampleBlock );

    Pa_Terminate();
    return 0;

xrun:
    printf("err = %d\n", err); fflush(stdout);
    if( stream ) {
       Pa_AbortStream( stream );
       Pa_CloseStream( stream );
    }
    free( sampleBlock );
    Pa_Terminate();
    if( err & paInputOverflow )
       fprintf( stderr, "Input Overflow.\n" );
    if( err & paOutputUnderflow )
       fprintf( stderr, "Output Underflow.\n" );
    return -2;
error1:
    free( sampleBlock );
error2:
    if( stream ) {
       Pa_AbortStream( stream );
       Pa_CloseStream( stream );
    }
    Pa_Terminate();
    fprintf( stderr, "An error occured while using the portaudio stream\n" );
    fprintf( stderr, "Error number: %d\n", err );
    fprintf( stderr, "Error message: %s\n", Pa_GetErrorText( err ) );
    return -1;
}
